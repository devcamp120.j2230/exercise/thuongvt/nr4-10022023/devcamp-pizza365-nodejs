// import thư viện mongose 
const mogoose = require("mongoose");

//import các model
const userModel = require("../model/UserModel");
const orderModel = require("../model/OrderModel");
const voucherModel = require("../model/VoucherModel");
const drinkModel = require("../model/DrinkModel");


//tạo odercode
const orderCode = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    let body = req.body;
   
    let orderCodeRandom = Math.random().toString().substring(2,5);
    body.orderCode = orderCodeRandom;

    // B2: Validate dữ liệu từ request body
    if (!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email chưa chính xác"
        })
    }
    // Sử dụng userModel tìm kiếm bằng email
    userModel.findOne({email: body.email},(err,user)=>{
        console.log(user)
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            if(!user){ // nếu dữ liệu là null hoặc undefine thì tạo mới một user mới
                userModel.create({
                    _id: mogoose.Types.ObjectId(),
                    fullName: body.fullName,
                    email: body.email,
                    address: body.address,
                    phone: body. phone,
                    status: body.status
                },(errUser,dataUsser)=>{
                    if(errUser){
                        return res.status(500).json({
                            message: errUser.message,
                            status:"Lỗi khi tạo mới User"
                        })
                    }
                    else{
                        // tạo một oder mới từ user vừa tạo
                        orderModel.create({
                            _id: mogoose.Types.ObjectId(),
                            orderCode: orderCodeRandom,
                            pizzaSize: body.pizzaSize,
                            pizzaType: body.pizzaType,
                            voucher: body.voucher,
                            drink: body.drink,
                            status: body.status
                        },(errCreatOrder, dataOrder)=>{
                            if(errCreatOrder){
                                return res.status(500).json({
                                    message:errCreatOrder.message,
                                    status: "không tạo mới được order nhé hfghfh"
                                })
                            }
                            else{
                                userModel.findByIdAndUpdate(dataOrder._id,
                                    {
                                    $push:{orders:dataOrder._id}
                                    },
                                    (err,data)=>{
                                        if(err){
                                            return res.status(500).json({
                                                message:"không thể tạo dữ liệu"
                                            })
                                        }
                                        else{
                                            return res.status(200).json({
                                                message:"tạo được dữ liệu order mới rồi nhé",
                                                data: dataOrder
                                            })
                                        }
                                    }
                                )
                            }
                        })
                    }
                })
            }
           else{ // nếu user tồn tại thì lấy id của user tạo order
            
            orderModel.create({
                _id: mogoose.Types.ObjectId(),
                orderCode: orderCodeRandom,
                pizzaSize: body.pizzaSize,
                pizzaType: body.pizzaType,
                voucher: body.voucher,
                drink: body.drink,
                status: body.status
                },(errorCreatedOrder, createdOrder)=>{
                if(errorCreatedOrder){
                    return res.status(500).json({
                        message: errorCreatedOrder.message,
                        status:"không tạo mới được order nhé huhu"
                    })
                }
                else{
                    userModel.findByIdAndUpdate(createdOrder._id,{$push:{orders:createdOrder._id}}).exec((err,data)=>{
                            if(err){
                                return res.status(500).json({
                                    message:"không thể tạo dữ liệu"
                                })
                            }
                            else{
                                return res.status(200).json({
                                    message:"tạo được dữ liệu order mới rồi nhé hehe haha",
                                    data: createdOrder
                                })
                            }
                        }
                    )
                }
            })
           } 
        }
    })

}

const getAllDrink = (req,res)=>{
    drinkModel.find().exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "all data get successfuly",
                dink: data
            })
        }
    })
}

module.exports = {
    orderCode,
    getAllDrink
}


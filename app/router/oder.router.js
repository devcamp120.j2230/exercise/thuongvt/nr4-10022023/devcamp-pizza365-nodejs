//Khai báo thư viện express
const express = require("express");
// Khai báo Middleware 
// Khai báo controller
const orderController = require("../controller/oders.controller")
//khai báo ruoter
const orderRouter = express.Router();

orderRouter.post("/devcamp-pizza365/orders",orderController.orderCode);
orderRouter.get("/devcamp-pizza365/drinks",orderController.getAllDrink)


module.exports = {orderRouter}
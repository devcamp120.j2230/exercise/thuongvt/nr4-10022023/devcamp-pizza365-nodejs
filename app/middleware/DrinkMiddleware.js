const getAllDrinkMiddleware = (req,res,next)=>{
    console.log("get all drink");
    next()
}

const getADrinkMiddleware = (req,res,next)=>{
    console.log("get a drink");
    next();
}

const postDrinkMiddleware = (req,res,next)=>{
    console.log("post a drink");
    next();
}

const putDrinkMiddleware = (req,res,next)=>{
    console.log("put a drink");
    next();
}

const deleteDrinkMiddleware = (req,res,next)=>{
    console.log("delete a drink");
    next();
}

module.exports = {
    getAllDrinkMiddleware,
    getADrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
}
